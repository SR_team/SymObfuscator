//
// Created by sr_team on 09.03.22.
//

#include "keep_symbols_t.h"

keep_symbols_t::keep_symbols_t( char **symbols, int count ) : symbols( symbols ), count( count ) {
}

bool keep_symbols_t::has_must_keeped( std::string_view symbol ) {
	for ( auto i = 0; i < count; ++i ) {
		std::string_view keep_symbol = symbols[i];
		if ( keep_symbol == symbol )
			return true;
		if ( keep_symbol.length() > 1 && keep_symbol.back() == '*' ) {
			if ( keep_symbol.front() == '*' ) {
				auto match = keep_symbol.substr( 1, keep_symbol.length() - 2 );
				if ( symbol.find( match ) != std::string_view::npos )
					return true;
			} else {
				auto match = keep_symbol.substr( 0, keep_symbol.length() - 1 );
				if ( symbol.starts_with( match ) )
					return true;
			}
		} else if ( keep_symbol.front() == '*' ) {
			auto match = keep_symbol.substr( 1 );
			if ( symbol.ends_with( match ) )
				return true;
		}
	}
	return false;
}
