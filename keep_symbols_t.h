//
// Created by sr_team on 09.03.22.
//

#pragma once

#include <string_view>

class keep_symbols_t {
	char **symbols;
	int count;

public:
	keep_symbols_t( char **symbols, int count );

	[[nodiscard]] bool has_must_keeped( std::string_view symbol);
};
