//
// Created by sr_team on 09.03.22.
//

#include "obf_name_generator_t.h"

#include <charconv>
#include <random>

obf_name_generator_t &obf_name_generator_t::get() {
	static obf_name_generator_t self;
	return self;
}

std::string obf_name_generator_t::generate( std::uint32_t len, std::uintptr_t address ) {
	std::string obf_name, base_name;
	if ( address != 0 && len >= 8 ) {
		obf_name.resize( 8, '\0' );
		std::to_chars( obf_name.data(), obf_name.data() + 8, address, 16 );
		auto str_end = obf_name.find( '\0' );
		if ( str_end != std::string::npos )
			obf_name.resize( str_end );
		if (obf_name.length() < 8)
			obf_name.insert(0, 1, '_');
		else if (obf_name.front() >= '0' && obf_name.front() <= '9')
			obf_name.front() = '_';
	}
	if ( obf_name.length() == len ) {
		auto it = std::find( generated_.begin(), generated_.end(), obf_name );
		if ( it == generated_.end() ) {
			generated_.push_back( obf_name );
			return obf_name;
		}
		obf_name.clear();
	} else if (!obf_name.empty())
		base_name = obf_name;

	std::default_random_engine generator;
	std::uniform_int_distribution<int> lower_case( 'a', 'z' ); // 0
	std::uniform_int_distribution<int> upper_case( 'A', 'Z' ); // 1
	std::uniform_int_distribution<int> numeric( '0', '9' ); // 2
	std::uniform_int_distribution<int> symbol_type( 0, 3 );
	while ( true ) {
		for ( auto i = base_name.length(); i < len; ++i ) {
			auto type = symbol_type( generator );
			switch ( type ) {
				case 0:
					obf_name.push_back( static_cast<char>( lower_case( generator ) ) );
					break;
				case 1:
					obf_name.push_back( static_cast<char>(upper_case( generator ) ) );
					break;
				case 2:
					obf_name.push_back( static_cast<char>(numeric( generator ) ) );
					break;
				default:
					obf_name.push_back( '_' );
					break;
			}
		}
		auto it = std::find( generated_.begin(), generated_.end(), obf_name );
		if ( it == generated_.end() ) {
			generated_.push_back( obf_name );
			return obf_name;
		}
		obf_name = base_name;
	}
}
