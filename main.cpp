#include <algorithm>
#include <charconv>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <vector>

#include <LIEF/LIEF.hpp>

#include "keep_symbols_t.h"
#include "obf_name_generator_t.h"

struct function_t {
    std::string name;
    std::uint64_t address = 0;
    std::uint64_t size = 1;
};

int main( int argc, char **argv ) {
    if ( argc < 2 || !std::filesystem::is_regular_file( argv[1] ) ) {
		std::cerr << "Usage to obfuscate: " << argv[0] << " <path to file> [keep symbols]" << std::endl;
		std::cerr << "Usage to restore: " << argv[0] << " <path to file> <patch to radare2 script>" << std::endl;
        return 1;
    }

    auto orig_size = std::filesystem::file_size( argv[1] );

    if ( argc == 3 && std::filesystem::is_regular_file( argv[2] ) ) {
        std::vector<function_t> functions;
        std::ifstream rz_script( argv[2] );
        std::string line;
        while ( std::getline( rz_script, line ) ) {
            if ( !line.starts_with( "f " ) ) continue;
            auto addr_pos = line.find( " @ " );
            if ( addr_pos == std::string::npos ) continue;
            auto name = line.substr( 2, addr_pos - 2 );
            auto addr = line.substr( addr_pos + 3 );

            std::uint64_t size = 1;
            auto size_pos = name.rfind( ' ' );
            if ( size_pos != std::string::npos ) {
                std::from_chars( &name[size_pos + 1], name.data() + name.length(), size );
                name.resize( size_pos );
            }

            std::uint64_t addr_bin = 0;
            std::from_chars( addr.data(), addr.data() + addr.length(), addr_bin );
            if ( addr_bin == 0 ) continue;

            functions.push_back( {name, addr_bin, size} );
        }

        auto binary = LIEF::ELF::Parser::parse( argv[1] );
        auto arch = binary->header().machine_type();
        for ( auto &fn : binary->exported_symbols() ) {
            auto it = std::find_if( functions.begin(), functions.end(), [arch, &fn]( const function_t &stored ) {
                auto address = fn.value();
                auto size = fn.size();
                if ( size == 0 ) size = 1;
                if ( ( arch == LIEF::ELF::ARCH::EM_ARM || arch == LIEF::ELF::ARCH::EM_AARCH64 ) && address % 2 != 0 ) address -= 1;

                return address == stored.address && size == stored.size && fn.name().length() == stored.name.length();
            } );
            if ( it == functions.end() ) {
                std::cerr << "Function " << fn.name() << " from " << fn.value() << " not restored" << std::endl;
                continue;
            }
            auto stored_fn = *it;
            functions.erase( it );

            fn.name() = std::move( stored_fn.name );
            fn.name().resize( fn.name().length() - 1 );
        }
        binary->write( argv[1] );

        if ( !functions.empty() ) std::cerr << "Unrestored symbols: " << functions.size() << std::endl;

        auto deobf_size = std::filesystem::file_size( argv[1] );
        if ( deobf_size != orig_size )
            std::cerr << "Broken file size after LIEF patching - " << orig_size << " != " << deobf_size << std::endl;
		return 0;
    }

	keep_symbols_t keep_symbols( &argv[2], argc - 2 );

	try {
		auto binary = LIEF::ELF::Parser::parse( argv[1] );
        std::ofstream rz_script( std::string( argv[1] ) + ".rz" );

        auto arch = binary->header().machine_type();

        for ( auto &fn : binary->exported_symbols() ) {
            if ( keep_symbols.has_must_keeped( fn.name().c_str() ) ) continue;

            auto address = fn.value();
            if ( ( arch == LIEF::ELF::ARCH::EM_ARM || arch == LIEF::ELF::ARCH::EM_AARCH64 ) && address % 2 != 0 ) address -= 1;
            rz_script << "f " << fn.name() << " " << ( fn.size() != 0u ? fn.size() : 1 ) << " @ " << address << std::endl;
			fn.name() = obf_name_generator_t::get().generate( fn.name().length(), fn.value() );
        }

        binary->write( argv[1] );
        auto obf_size = std::filesystem::file_size( argv[1] );
        if ( obf_size != orig_size ) std::cerr << "Broken file size after LIEF patching - " << orig_size << " != " << obf_size << std::endl;

    } catch ( const LIEF::exception &err ) { std::cerr << err.what() << std::endl; }

	return 0;
}
