//
// Created by sr_team on 09.03.22.
//

#pragma once

#include <deque>
#include <string>
#include <cstdint>

class obf_name_generator_t {
	std::deque<std::string> generated_;

	obf_name_generator_t() = default;
	obf_name_generator_t( obf_name_generator_t & ) = default;
	obf_name_generator_t( obf_name_generator_t && ) = default;
	~obf_name_generator_t() = default;

public:
	static obf_name_generator_t &get();
	std::string generate( std::uint32_t len, std::uintptr_t address = 0 );
};
